# Linux Hands-on

1. fork the repo
1. create a unique dir inside `1-solutions/` directory
1. copy the exercise file (.md)
1. add your solution using **markdown** syntax
1. commit - push to your repo
1. raise pull request, if you want to submit your solution
1. there may be delay in reviewing the pull req
1. pull request will be merged if the solution is unique
